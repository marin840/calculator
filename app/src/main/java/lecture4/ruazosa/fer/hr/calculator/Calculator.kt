package lecture4.ruazosa.fer.hr.calculator

import kotlin.math.exp

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "X" -> expression.add(operator)
            "/" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {

        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }

        var priorityExpression: MutableList<String> = mutableListOf()

        result = expression[0].toDouble()

        //for (i in 1..expression.count() - 1 step 2) {
         //   when (expression[i]) {
                //"+" -> result = result + expression[i+1].toDouble()
                //"-" -> result = result - expression[i+1].toDouble()
                //"X" -> result = result * expression[i+1].toDouble()
                //"/" -> result = result / expression[i+1].toDouble()
                //"+" -> {priorityExpression.add(expression[i-1])
                //       priorityExpression.add("+")
                //      if(i == expression.count()-1) priorityExpression.add(expression[i+1])}
                //"-" -> {priorityExpression.add(expression[i-1])
                //       priorityExpression.add("-")
         //   }

        //}
        var a = 1
        var pom = expression[0]
        while (a < expression.count()) {
            when (expression[a]) {
                "+" -> {
                    priorityExpression.add(pom)
                    pom = expression[a + 1]
                    a = a + 2
                    priorityExpression.add("+")
                }

                "-" -> {
                    priorityExpression.add(pom)
                    pom = expression[a + 1]
                    a = a + 2
                    priorityExpression.add("-")
                }

                "X", "/" -> {
                    while ((expression[a] == "X" || expression[a] == "/")) {
                        if (expression[a] == "X") pom = (pom.toDouble() * expression[a + 1].toDouble()).toString()
                        if (expression[a] == "/") pom = (pom.toDouble() / expression[a + 1].toDouble()).toString()

                        a = a + 2
                        if(a > expression.count()-1) break

                    }
                }

            }
        }
        priorityExpression.add(pom)

        if(priorityExpression.count() == 0) result = pom.toDouble()
            else result = priorityExpression[0].toDouble()

        for (i in 1..priorityExpression.count() - 1 step 2) {
            when (priorityExpression[i]) {
                "+" -> result = result + priorityExpression[i + 1].toDouble()
                "-" -> result = result - priorityExpression[i + 1].toDouble()
            }
        }
    }
}